# Changelog

## Unreleased

## 3.0.2  - 2023-06-09

### Changed

- Compatibilité SPIP 4.2
- ajout d'un CHANGELOG.md
