<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-seo?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'seo_description' => 'SEO - це плагін, який додає можливість вставляти в частину head вашого веб-сайту: мета-теги, канонічні URL, мета-код інструментів веб-мастера Google та JavaScript Google Analytics. Він налаштовується на сторінці конфігурації SPIP, а також в кожній рубриці та статті для мета-тегів.',
	'seo_nom' => 'SEO',
	'seo_slogan' => 'Плагін для пошукової оптимізації сайту'
);
