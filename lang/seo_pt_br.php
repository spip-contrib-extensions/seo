<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/seo?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'S.E.O' => 'SEO',

	// A
	'alexa' => 'Alexa',
	'alexa_activate' => 'Ativar o Alexa',
	'alexa_id' => 'Identificador do site para o Alexa',

	// B
	'bing_webmaster' => 'Bing Webmaster Tools',
	'bing_webmaster_activate' => 'Ativar as Bing Webmaster Tools',
	'bing_webmaster_id' => 'Meta código de verificação',

	// C
	'canonical_url' => 'URLs Canônico',
	'canonical_url_activate' => 'Ativar a meta do URL Canônico',

	// E
	'explication_description' => 'É recomendável que o tamanho seja inferior ou igual a 160 caracteres.',
	'explication_title' => 'É recomendável que o tamanho seja entre 5 e 70 caracteres.',

	// F
	'forcer_squelette_descriptif' => 'As metas SEO são prioritárias em relação às metas gerais dos templates',
	'forcer_squelette_label' => 'Carregar as metas para todos os templates',

	// G
	'google_analytics' => 'Google Analytics',
	'google_analytics_activate' => 'Ativar o Google Analytics',
	'google_analytics_id' => 'Google Analytics web property ID',
	'google_analytics_universal' => 'Usar <a href="https://support.google.com/analytics/answer/2790010?hl%3Den">Google Analytics Universal</a>',
	'google_webmaster_tools' => 'Google Webmaster Tools',
	'google_webmaster_tools_activate' => 'Ativar o Google Webmaster Tools',
	'google_webmaster_tools_id' => 'Meta código de verificação',

	// I
	'info_count_max' => 'Caracteres para atingir ao limite ideal: ',
	'insert_head' => 'Inserção automática em #INSERT_HEAD',
	'insert_head_activate' => 'Ativar a inserção automática',
	'insert_head_descriptif' => 'Inserção automática da configuração SEO no &lt;head&gt;',

	// M
	'meta_author' => 'Autor:',
	'meta_copyright' => 'Copyright:',
	'meta_description' => 'Descrição:',
	'meta_keywords' => 'Palavras-chaves:',
	'meta_page_description_sommaire_value' => 'Valor da descrição da Página + Valor Meta do Resumo',
	'meta_page_description_value' => 'Valor da Descrição da Página',
	'meta_page_title_sommaire_value' => 'Valor do Título da Página + Valor Meta da Página Inicial',
	'meta_page_title_value' => 'Valor do Título da Página',
	'meta_robots' => 'Robôs:',
	'meta_sommaire_value' => 'Valor Meta da Página Inicial',
	'meta_tags' => 'Meta Tags',
	'meta_tags_activate' => 'Ativar as meta tags editoriais',
	'meta_tags_default' => 'Valor das meta tags padrão (para os objetos editoriais)',
	'meta_tags_edit_activate' => 'Ativar a edição das meta tags nos objetos editoriais',
	'meta_tags_editing' => 'Edição das meta tags',
	'meta_tags_sommaire' => 'Valor das meta tags da página inicial (home)',
	'meta_title' => 'Título:',

	// S
	'seo' => 'Search Engine Optimisation'
);
