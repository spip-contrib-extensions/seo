<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/seo?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'S.E.O' => 'SEO',

	// A
	'alexa' => 'Alexa',
	'alexa_activate' => 'Aktivovať Alexu',
	'alexa_id' => 'Identifikátor stránky na Alexu',

	// B
	'bing_webmaster' => 'Bing Webmaster Tools',
	'bing_webmaster_activate' => 'Aktivovať Bing Webmaster Tools',
	'bing_webmaster_id' => 'Meta kód na overenie',

	// C
	'canonical_url' => 'Kanonická podoba adries',
	'canonical_url_activate' => 'Aktivovať kanonické adresy URL',

	// E
	'explication_description' => 'Odporúča sa, aby mal 160 znakov alebo menej.',
	'explication_title' => 'Odporúča sa, aby mal od 5 do 70 znakov.',

	// F
	'forcer_squelette_descriptif' => 'Hlavičky meta zo SEO majú prednosť pred tými, ktoré vygenerujú šablóny',
	'forcer_squelette_label' => 'Nahrať hlavičky meta pre všetky šablóny',

	// G
	'google_analytics' => 'Google Analytics',
	'google_analytics_activate' => 'Aktivovať Google Analytics',
	'google_analytics_id' => 'ID webového objektu Google Analytics ',
	'google_webmaster_tools' => 'Nástroje Google Webmaster Tools',
	'google_webmaster_tools_activate' => 'Aktivovať Google Webmaster Tools',
	'google_webmaster_tools_id' => 'Kontrolný kód',

	// I
	'info_count_max' => 'Počet znakov na dosiahnutie optimálneho limitu:',
	'insert_head' => 'Automatické vkladanie do #INSERT_HEAD',
	'insert_head_activate' => 'Aktivovať automatické vkladanie',
	'insert_head_descriptif' => 'Automatické vkladanie nastavenia SEO do &lt;hlavičky&gt;',

	// M
	'meta_author' => 'Autor(ka):',
	'meta_copyright' => 'Autorské práva:',
	'meta_description' => 'Opis:',
	'meta_keywords' => 'Kľúčové slová:',
	'meta_page_description_sommaire_value' => 'Hodnota opisu stránky + hodnota tagu Zhrnutie',
	'meta_page_description_value' => 'Hodnota opisu stránky',
	'meta_page_title_sommaire_value' => 'Hodnota nadpisu stránky + hodnota tagu Zhrnutie',
	'meta_page_title_value' => 'Hodnota nadpisu stránky',
	'meta_robots' => 'Roboty:',
	'meta_sommaire_value' => 'Hodnota tagu Zhrnutie',
	'meta_tags' => 'Meta tagy',
	'meta_tags_activate' => 'Aktivovať redakčné meta tagy',
	'meta_tags_default' => 'Predvolená hodnota meta tagov (pre články a rubriky)', # MODIF
	'meta_tags_edit_activate' => 'Aktivovať úpravu meta tagov v rubrikách a článkoch', # MODIF
	'meta_tags_editing' => 'Úprava meta tagov',
	'meta_tags_sommaire' => 'Hodnota meta tagov zhrnutia (úvodná stránka)',
	'meta_title' => 'Nadpis:',

	// S
	'seo' => 'Optimalizácia pre vyhľadávače'
);
